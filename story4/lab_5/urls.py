from django.urls import path
from . import views

urlpatterns = [
    path(' ', views.schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete')
]
