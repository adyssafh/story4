from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.

from .models import Schedule
from . import forms


def profile(request):
	return render(request,'profile.html')

def home(request):
	return render(request,'home.html')

def contact(request):
	return render(request,'contact.html')

def more(request):
	return render(request,'more.html')

def portofolio(request):
	return render(request,'portofolio.html')

def navbar(request):
	return render(request,'nav.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    form = forms.ScheduleForm(request.POST) 
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
    Schedule.objects.all().delete()
    return render(request, "schedule.html")

