from django.urls import include, path
from .views import *
from django.urls import path
from . import views


urlpatterns = [
    path('profile/', profile, name='profile'),
    path('', home, name = 'home'),
    path('contact/', contact, name = 'contact'),
    path('more/', more, name = 'more'),
    path('portofolio/', portofolio, name = 'portofolio'),
    path('navbar/', navbar, name='navbar'),
    path('schedule/', schedule, name='schedule'),
    path('schedule/create', views.schedule_create, name='schedule_create'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete')
]